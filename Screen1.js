import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View } from 'react-native';

const instructions = Platform.select({
    ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
    android:
        'Double tap R on your keyboard to reload,\n' +
        'Shake or press menu button for dev menu',
});

type Props = {};
export default class App extends Component<Props> {
    render() {
        return (

            <View style={styles.container}>


                <View style={styles.content}>

                    <View style={styles.proImg}>
                        <Text style={styles.headerText}>Image</Text>
                    </View>

                    <View style={styles.box}>
                        <Text style={styles.headerText}>TextInput</Text>
                    </View>

                    <View style={styles.box}>
                        <Text style={styles.headerText}>TextInput</Text>
                    </View>

                    <View style={styles.boxTouch}>
                        <Text style={styles.headerText}>TouchableOpacity</Text>
                    </View>

                </View>
            </View>

        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'steelblue',
    },
    content: {
        flexDirection: 'column',
        justifyContent: 'center',
        backgroundColor: 'steelblue',
        flex: 1,
    },
    box: {
        backgroundColor: 'powderblue',
        width: 250,
        height: 50,
        margin: 14
    },
    boxTouch: {
        backgroundColor: 'skyblue',
        width: 250,
        height: 50,
        margin: 14,
        
    },
    header: {
        alignItems: 'center',
        backgroundColor: 'skyblue',
    },
    headerText: {
        color: 'white',
        fontSize: 20,
        fontWeight: 'bold',
        padding: 10,
    },
    proImg: {
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'powderblue',
        borderRadius: 150,
        width: 200,
        height: 200,
        margin: 40
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    }

});
