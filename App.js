/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View } from 'react-native';

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

type Props = {};
export default class App extends Component<Props> {
  render() {
    return (

      <View style={styles.container}>
        <View style={styles.header}>
          <Text style={styles.headerText}>News</Text>
        </View>

        <View style={styles.content}>
          <View style={styles.box}>
            <Text style={styles.headerText}>Lorem</Text>
          </View>

          <View style={styles.box}>
            <Text style={styles.headerText}>Lorem</Text>
          </View>
        </View>
        
        <View style={styles.content}>
          <View style={styles.box}>
            <Text style={styles.headerText}>Lorem</Text>
          </View>

          <View style={styles.box}>
            <Text style={styles.headerText}>Lorem</Text>
          </View>
        </View>

        <View style={styles.content}>
          <View style={styles.box}>
            <Text style={styles.headerText}>Lorem</Text>
          </View>

          <View style={styles.box}>
            <Text style={styles.headerText}>Lorem</Text>
          </View>
        </View>
      </View>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: 'steelblue',
  },
  content: {
    justifyContent: 'center',
    backgroundColor: 'steelblue',
    flexDirection: 'row',
    flex: 1,  
  },
  box: {
    backgroundColor: 'powderblue',
    width :150,
    height: 150,
    margin: 14
  },
  header: {
    alignItems: 'center',
    backgroundColor: 'skyblue',
  },
  headerText: {
    color: 'white',
    fontSize: 20,
    fontWeight: 'bold',
    padding: 30,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  }

});
