/**
 * @format
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import {AppRegistry} from 'react-native';
import App from './App';
import Screen1 from './Screen1';
import Screen2 from './Screen2';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => Screen2);
