import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View } from 'react-native';

const instructions = Platform.select({
    ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
    android:
        'Double tap R on your keyboard to reload,\n' +
        'Shake or press menu button for dev menu',
});

type Props = {};
export default class App extends Component<Props> {
    render() {
        return (

            <View style={styles.container}>
                <View style={styles.header}>
                    <View style={styles.box}>
                        <Text style={styles.headerText}>1</Text>
                    </View>

                    <View style={styles.headerMainBox}>
                        <Text style={styles.headerText}>Home</Text>
                    </View>

                    <View style={[styles.box]}>
                        <Text style={styles.headerText}>2</Text>
                    </View>

                </View>

                <View style={styles.content}>
                    <Text style={styles.headerText}>ScrollView</Text>
                </View>

                <View style={styles.footer}>
                    <View style={styles.box}>
                        <Text style={styles.headerText}>I</Text>
                    </View>

                    <View style={styles.box}>
                        <Text style={styles.headerText}>C</Text>
                    </View>
                    <View style={styles.box}>
                        <Text style={styles.headerText}>O</Text>
                    </View>
                    <View style={styles.box}>
                        <Text style={styles.headerText}>N</Text>
                    </View>
                </View >
            </View >

        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: 'steelblue',
    },
    content: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'steelblue',
        flexDirection: 'row',
        flex: 1,
    },
    box: {
        backgroundColor: 'powderblue',
        width: 95,
        height: 95,
        margin: 4
    },
    header: {
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: 'skyblue',
        height: 70,
    },
    headerMainBox: {
        flexDirection: 'column',
        alignItems: 'center',
        backgroundColor: 'skyblue',
        width: 210,
        height: 70,
    },
    leftFlex: {
        justifyContent: 'flex-end',
    },
    headerText: {
        color: 'white',
        fontSize: 20,
        fontWeight: 'bold',
        padding: 30,
    },
    
    centerText:{
        justifyContent: 'center',
        alignItems: 'center',
    },
    footer: {
        flexDirection: 'row',
        alignItems: 'flex-end',
        backgroundColor: 'skyblue',
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    }

});
